# Kudos

An operating system for ComputerCraft and open computers. Inspired by Emacs and written in Urn.

## Building
Assuming `tacky/cli.lua` is your Urn executable, you should run:

```sh
> tacky/cli.lua -i platforms/cc kudos/init -o kudos
```

then simply run `kudos.lua` on your computer.
