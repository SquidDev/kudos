(defun create-key (key mode)
  "Create a new key binding using KEY and MODE."
  { :key key :mode (or mode "n") })

(defun create-group ()
  "Create a new key chord group."
  { :tag "group"
    :n   {}
    :c   {}
    :m   {}
    :cm  {} })

(defun add-key! (group chord func)
  "Register a new key CHORD under the given GROUP."
  (assert-type! group group)
  (assert-type! chord list)
  (assert-type! func function)
  (when (empty? chord) (fail! "Cannot register empty chord"))

  (for i 1 (pred (n chord)) 1
    (let* [(elem (nth chord i))
           (next (.> group (.> elem :mode) (.> elem :key)))]
      (cond
        [(= next nil)
         (set! next (create-group))
         (.<! group (.> elem :mode) (.> elem :key) next)]
        [(= (type next) "function")
         (fail! (.. "Cannot register keybinding, " (.> elem :mode) "-" (.> elem :key) " is a function."))]
        [true])
      (set! group next)))

  (with (elem (nth chord (n chord)))
    (when (.> group (.> elem :mode) (.> elem :key))
      (fail! (.. "Cannot register keybinding, " (.> elem :mode) "-" (.> elem :key) " is already taken.")))
    (.<! group (.> elem :mode) (.> elem :key) func)))

(defun handle-key! (group key ctrl meta)
  "Handle pressing KEY with CTRL and META modifiers in the given GROUP.
   Returns the next group, or nil if none is needed."
  (assert-type! group group)
  (with (res (.> group (cond
                         [(and ctrl meta) "cm"]
                         [ctrl "c"]
                         [meta "m"]
                         [true "n"]) key))
    (cond
      [(= res nil) false]
      [(invokable? res)
       (res)
       nil]
      [true res])))
