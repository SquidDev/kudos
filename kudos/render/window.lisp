(import kudos/buffer ())
(import kudos/render/colours colours)
(import kudos/render/core render)

(defun render-buffer (window buffer)
  "Render the given BUFFER, located in the specified WINDOW."
  (render/set-position! (.> window :x) (.> window :y))
  (render/set-back! colours/white)

  (when buffer
    ((.> (buffer-major-mode buffer) :render) window buffer))

  (unless (.> window :mini)
    (render/set-position! (.> window :x) (pred (+ (.> window :y) (.> window :height))))
    (render/set-back! colours/grey)
    (render/set-fore! colours/light-grey)
    (render/write! (string/rep " " (.> window :width)))))

(defun render-window (window)
  "Render the given WINDOW."
  (case (.> window :mode)
    ["buffer"
     (render-buffer window (.> window :buffer))]
    ["root"
     (with (elem (.> window :root))
       (assert-type! elem window)
       (render-window elem))]
    ["hchild"
     (with (elem (.> window :hchild))
       (assert-type! elem window)
       (while elem
         (render-window elem)
         (set! elem (.> elem :next))
         (when elem
           (render/set-fore! colours/light-grey)
           (render/set-back! colours/black)
           (for y 0 (pred (.> window :height)) 1
             (render/set-position! (pred (.> elem :x)) (+ y (.> elem :y)))
             (render/write! "|")))))]
    ["vchild"
     (with (elem (.> window :vchild))
       (assert-type! elem window)
       (while elem
         (render-window elem)
         (set! elem (.> elem :next))))]))
