"Data structures for storing positions and ranges in a file, as well as
 various methods for working with them."

(import data/struct ())

(defstruct (position position-of position?)
  "Create a new position from a LINE and COLUMN."
  (fields
    (immutable line   pos-line
      "The line this position exists at")
    (immutable column pos-col
      "The column this position exists at")))

(defmethod (pretty position) (pos)
  (string/format "%d:%d" (pos-line pos) (pos-col pos)))

(defmethod (eq? position position) (a b)
  (and (= (pos-line a) (pos-line b)) (= (pos-col a) (pos-col b))))

(defun p< (a b)
  "Determine whether position A comes before B."
  (if (= (pos-line a) (pos-line b))
    (< (pos-col a) (pos-col b))
    (< (pos-line a) (pos-col b))))

(defstruct (range range-of range?)
  "Create a range from a START-LINE, START-COL, END-LINE and END-COL
   determining whether the selection is inverted or not."
  (fields
    (immutable inverse range-inv
      "Whether this range is inverted. Namely selection starts at the
       bottom right instead of top left.")
    (immutable start-line
      "The start line of this range. Note the start is always before the
       end.")
    (immutable start-col
      "The start column of this range.")
    (immutable end-line
      "The end line of this range.")
    (immutable end-col
      "The end column of this range."))
  (constructor new
    (lambda (start-line start-col end-line end-col)
      (if (if (= start-line end-line)
            (< start-col end-col)
            (< start-line end-line))
        (new false start-line start-col end-line end-col)
        (new true  end-line end-col start-line start-col)))))

(defmethod (pretty range) (range)
  (string/format "%d:%d%s%d:%d"
                 (range-start-line range) (range-start-col range)
                 (if (range-inv range) "<-" "->")
                 (range-end-line   range) (range-end-col   range)))

(defun range-of-positions (start end)
  "Create a range from a START and END position, determining whether the
   selection is inverted or not."
  (range-of (pos-line start) (pos-col start)
            (pos-line end) (pos-col end)))

(defun range-start (range)
  "Get the start position for a given RANGE."
  (position-of (range-start-line range) (range-start-col range)))

(defun range-end (range)
  "Get the end position for  a given RANGE."
  (position-of (range-end-line range) (range-end-col range)))

(defun range-cursor (range)
  "Get the cursor position from a given RANGE."
  (if (range-inv range)
    (range-start range)
    (range-end range)))


position-of
