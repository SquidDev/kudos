"Various data structures for storing text in a file, as well as
 associated annotations and metadata."

(import data/struct ())

(import kudos/model/range ())

(defstruct (line (hide line-of) line?)
  "Stores a line in the given text file."
  (fields
    (mutable contents
      "The contents of this line.")))

(defstruct (text-model text-model-of text-model?)
  "Create a text model of the given STRING."
  (fields
    (immutable lines (hide tm-lines)
      "A list of lines for this text model")
    (immutable undo-stack (hide tm-undo-stack)
      "A stack of modifications which have been made to this buffer."))

  (constructor new
    (lambda (string)
      (new (map line-of (string/split string "\n")) '()))))

(defun push-edit! (model range string data)
  "Insert a STRING into the current MODEL, overwriting the text stored at
   RANGE.  This will be pushed on the undo stack along with the
   associated DATA."
  (assert-type! model text-model)
  (assert-type! range range)
  (assert-type! string string)

  (with (change (apply-edit! model range string))
    (.<! change :data data)
    (push-cdr! (tm-undo-stack model) change)))

(defun apply-edit! (model range string)
  "Insert a STRING into the current MODEL, overwriting the text stored at
   RANGE. This returns the appropriate transformation required to undo
   this modification."
  (assert-type! model text-model)
  (assert-type! range range)
  (assert-type! string string)

  (let* [(lines (tm-lines model))
         (new-lines (string/split string "\n"))

         (r-start (range-start-line range))
         (r-end (range-end-line range))
         (del-lines (+ (- r-end r-start) 1))
         (ins-lines (n new-lines))
         (edit-lines (math/min del-lines ins-lines))

         (old-lines '())]

    (for line-no r-start r-end 1
      (let* [(start-col (if (= line-no r-start) (range-start-col range)       1))
             (end-col   (if (= line-no r-end)   (pred (range-end-col range)) -1))]
        (push-cdr! old-lines (string/sub (line-contents (nth lines line-no))
                                         start-col end-col))))

    ;; Set all bar the last line
    (for i 1 (pred edit-lines) 1
      (let* [(line-no (+ r-start i -1))

             (edit-line (nth lines line-no))
             (edit-contents (line-contents edit-line))

             (ins-contents (nth new-lines i))

             (start-col (if (= line-no r-start)
                          (pred (range-start-col range))
                          0))
             (end-col   (if (= line-no r-end)
                          (range-end-col range)
                          (succ (n edit-contents))))]

        (set-line-contents! edit-line (.. (string/sub edit-contents 1 start-col)
                                          ins-contents
                                          (string/sub edit-contents end-col)))))

    (cond
      [(< edit-lines del-lines)
       ;; If we've got to delete some lines then append the last line of the selection
       ;; to the last line of inserted text
       (let* [(line-no (+ r-start edit-lines -1))
              (edit-line (nth lines line-no))
              (edit-contents (line-contents edit-line))
              (ins-contents (nth new-lines edit-lines))
              (start-col (if (= line-no r-start)
                           (pred (range-start-col range))
                           0))]

         (set-line-contents! edit-line (.. (string/sub edit-contents 1 start-col)
                                           ins-contents
                                           (string/sub (line-contents (nth lines r-end))
                                                       (range-end-col range)))))

       ;; And actually delete remaining lines
       (for i del-lines (succ edit-lines) -1
         (remove-nth! lines (succ edit-lines)))]

      [(< edit-lines ins-lines)
       ;; Append to the text before the insertion
       (let* [(line-no r-end)
              (edit-line (nth lines line-no))
              (edit-contents (line-contents edit-line))
              (ins-contents (nth new-lines edit-lines))
              (start-col (if (= line-no r-start)
                           (pred (range-start-col range))
                           0))]
         (set-line-contents! edit-line (.. (string/sub edit-contents 1 start-col)
                                           ins-contents))

         ;; Insert the intermediate lines
         (for i (succ edit-lines) (pred ins-lines) 1
           (insert-nth! lines (+ r-start i -1) (line-of (nth new-lines i))))

         ;; And insert the last text, followed by the rest of the previous line
         (insert-nth! lines (+ r-start ins-lines -1)
                            (line-of (.. (nth new-lines ins-lines)
                                         (string/sub edit-contents (range-end-col range))))))]

      [else
       (let* [(line-no r-end)
              (edit-line (nth lines r-end))
              (edit-contents (line-contents edit-line))
              (ins-contents (nth new-lines edit-lines))
              (start-col (if (= line-no r-start)
                           (pred (range-start-col range))
                           0))]
         (set-line-contents! edit-line (.. (string/sub edit-contents 1 start-col)
                                           ins-contents
                                           (string/sub edit-contents (range-end-col range)))))])

    { :range  (range-of r-start                  (range-start-col range)
                        (+ r-start ins-lines -1) (if (= ins-lines 1)
                                                   (+ (range-start-col range) (n (car new-lines)))
                                                   (+ (n (last new-lines)) 1)))
      :string (concat old-lines "\n") }))

(defun can-undo? (model)
  "Determine whether MODEL can undo any edits made."
  (assert-type! model text-model)
  (not (empty? (tm-undo-stack model))))

(defun undo-edit! (model)
  "Undo an edit on MODEL, returning the data provided as an argument to
   [[push-edit!]]."
  (assert-type! model text-model)
  (with (change (pop-last! (tm-undo-stack model)))
    (apply-edit! model (.> change :range) (.> change :string))
    (.> change :data)))

(defun tm-line-count (model)
  (n (tm-lines model)))

(defun tm-line (model n)
  (nth (tm-lines model) n))

(defun text-model->string (model)
  "Convert a text MODEL to a string."
  (assert-type! model text-model)
  (string/concat (map line-contents (tm-lines model)) "\n"))
