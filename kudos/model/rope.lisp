"This implements a [rope data structure](https://en.wikipedia.org/wiki/Rope_(data_structure))
 in Urn."

(import kudos/debug ())

(define join-length
  "The threshold at which a node will join its two child nodes together."
  32)

(define split-length
  "The maximum length a leaf's contents can be. Anything above this will
   be split into separate nodes."
  ;; TODO: Implement this
  1024)

(define edit-split-length
  "The maximum length a leaf's contents can be when editing. When an edit
   operation occurs and the length is greater than this, the node will be
   split."
  ;; TODO: Implement this
  32)

(define rebalance-ratio
  "The maximum ratio of left to right weight (or vice virsa). Anything
   larger than this will trigger a balancing of the tree."
  ;; TODO: Implement this
  1.2)

(defun length (x)
  "Determine the length of rope X."
  (.> x :length))

(defun height (x)
  "Determine the height of rope X."
  (.> x :height))

(defun empty? (x)
  "Determine whether rope X is empty."
  (with (value (.> x :value))
    (= (n value) 0)
    false))

(defun new-string (str)
  "Create a new rope from STR. This is an unsave method as
   you do not want to enable making big leaves."
  :hidden
  (debug-assert-type! str string)
  { :value str :length (n str) :height 0 })

(defun new-cons-len (len left right)
  "Create a new cons using LEFT and RIGHT using the given LEN."
  :hidden
  (debug-affirm (number? len)
                (= len (+ (length left) (length right))))
  { :length len
    :height (succ (math/max (height left) (height right)))
    :left   left
    :right  right })

(defun new-cons (left right)
  "Create a new cons using LEFT and RIGHT."
  :hidden
  { :length (+ (.> left :length) (.> right :length))
    :height (succ (math/max (height left) (height right)))
    :left   left
    :right  right })

(defun rope->string# (rope out)
  :hidden
  (with (value (.> rope :value))
    (if value
      (push-cdr! out value)
      (progn
        (rope->string# (.> rope :left) out)
        (rope->string# (.> rope :right) out)))))

(defun rope->string (rope)
  "Convert ROPE to a string."
  (with (out '())
    (rope->string# rope out)
    (concat out)))

(defun rope->debug# (rope out indent)
  :hidden
  (with (value (.> rope :value))
    (if value
      (with (quot (if (> (n value) 50)
                    (.. (string/quoted (string/sub 1 value 47)) "...")
                    (string/quoted value)))
        (push-cdr! out (.. indent (length rope) "  " quot "\n")))
      (progn
        (push-cdr! out (.. indent (length rope) "  " (height rope) "\n"))
        (rope->debug# (.> rope :left) out (.. indent "  "))
        (rope->debug# (.> rope :right) out (.. indent "  "))))))

(defun rope->debug (rope)
  "Convert ROPE into a debugging visualisation."
  (with (out '())
    (rope->debug# rope out "")
    (concat out)))

(defun of-substring (string start length)
  "Take a substring of STRING starting and START and going to START + LENGTH - 1.
   This simply partitions the string to generate a balenced tree."
  (debug-affirm (string? string)
                (number? start) (>= start 1)
                (number? length) (>= length 0) (<= (pred (+ start length)) (n string)))

  (if (<= length join-length)
    (new-string (string/sub string start (pred (+ start length))))
    (with (half-len (math/floor (/ length 2)))
      (debug-affirm (<= start (n string)))
      (new-cons-len length
        (of-substring string start half-len)
        (of-substring string (+ start half-len) (- length half-len))))))

(defun string->rope (string)
  "Convert the given STRING to a ROPE."
  (of-substring string 1 (n string)))

(defmacro for-rope (var rope &body)
  "Iterate over each string in ROPE, binding it to VAR and executing
   BODY."
  (assert-type! var symbol)
  (let* [(rope-var (gensym))
         (lam-var (gensym))
         (val-var (gensym))]
    `((lambda (,lam-var)
       (set! ,lam-var
         (lambda (,rope-var)
           (with (,var (.> ,rope-var :value))
             (cond
               [,var ,@body]
               [true
                (,lam-var (.> ,rope-var :left))
                (,lam-var (.> ,rope-var :right))]))))
       (,lam-var ,rope)))))

(defun iter-rope (fun rope)
  "Call FUN with each string in ROPE in order."
  (with (value (.> rope :value))
    (cond
      [value (fun value)]
      [true
       (iter-rope fun (.> rope :left))
       (iter-rope fun (.> rope :right))])))

(defun concat-balanced (left right)
  "Concats two nodes, adjusting the the tree structure, so that very long
   nodes are split and short ones are joined. This assumes that LEFT and
   RIGHT are roughy the same height."
  (let* [(left# (length left))
         (right# (length right))]

    (cond
      [(= left# 0) right]
      [(= right# 0) left]
      [(< (+ left# right#) join-length)
       (new-string (.. (.> left :value) (.> right :value)))]
      [true
       (let* [(leftl (.> left :left))
              (leftr (.> left :right))
              (rightl (.> right :left))
              (rightr (.> right :right))]
         (cond
           ;; (Node (Node a b) c) where height a >= height b and height a > height c
           ;;   => (Node (Node a b) c)
           [(and leftl (>= (height leftl) (height leftr)) (> (height leftl) (height right)))
            (new-cons leftl (new-cons leftr right))]

           ;; (Node a (Node b c)) where height c >= height b && height c > height a
           ;;   => (Node (Node a b) c)
           [(and rightr (>= (height rightr) (height rightl)) (> (height rightr) (height left)))
            (new-cons (new-cons left rightl) rightr)]

           ;; (Node (Node a lr@(Node b c)) d) where height lr@(Node b c) > height d
           ;;   => (Node (Node a b) (Node c d))
           ;; TODO: Implement me

           ;; (Node a (Node rl@(Node b c) d)) where height rl@(Node b c) > height a
           ;;  => (Node (Node a b) (Node c d))
           ;; TODO: Implement me

           ;; Otherwise
           [true (new-cons left right)]))])))

(defun remove (rope start end)
  "Remove the region between START and END in ROPE,
   creating a modified version."
  (debug-affirm (number? start) (>= start 1) (<= start (length rope))
                (number? end)   (>= end   1) (<= end   (length rope))
                (<= start end))

  (with (value (.> rope :value))
    (if value
      (new-string (.. (string/sub value 1 (pred start)) (string/sub value (succ end))))
      (let* [(left (.> rope :left))
             (right (.> rope :right))
             (left-length (length left))]

        (when (<= start left-length)
          (set! left (remove left start (math/min left-length end))))

        (when (> end left-length)
          (set! right (remove right (math/max 1 (- start left-length)) (- end left-length))))

        (concat-balanced left right)))))

(defun insert (rope pos str)
  "Insert a STR at POS into ROPE."
  (debug-affirm (number? pos) (>= pos 1) (<= pos (succ (.> rope :length)))
                (string? str) (<= (n str) split-length))

  (with (value (.> rope :value))
    (if value
      (string->rope (.. (string/sub value 1 (pred pos)) str (string/sub value pos)))
      (let* [(left (.> rope :left))
             (right (.> rope :right))
             (left-len (.> left :length))]
        (if (<= pos left-len)
          (set! left  (insert left pos str))
          (set! right (insert right (- pos left-len) str)))

        (concat-balanced left right)))))
