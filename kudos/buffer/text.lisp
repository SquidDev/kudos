(import kudos/debug (when-debug))
(import kudos/render/colours colours)
(import kudos/render/core render)
(import kudos/model/rope (for-rope))
(import kudos/model/rope rope)
(import kudos/buffer ())

(import test/assert (affirm))

(define text-mode
  { :render (lambda (window buffer)
              (let* [(x 1)
                     (y 1)
                     (total-offset 1)
                     ;; (idx 0)
                     ;; (ctr 0)
                     (cursors (.> window :cursors))
                     (cursor-idx 1)
                     (next-cursor (or (nth cursors cursor-idx) math/huge))
                     (width (.> window :width))
                     (height (.> window :height))
                     (contents (buffer-model buffer))]

                (render/set-fore! colours/black)
                (render/set-back! colours/white)

                (for-rope str contents
                  (let [(offset 1)
                        (len (n str))]

                    (while (and (<= offset len) (<= y height))
                      (let [(new-line (string/find str "\n" offset))
                            (cursor (succ (- next-cursor total-offset)))
                            (line-end (+ offset (- width x)))
                            (mode "norm")

                            (end len)]

                        (when (<= cursor end)
                          (set! mode "curs")
                          (set! end (pred cursor)))
                        (when (<= line-end end)
                          (set! mode "lend")
                          (set! end line-end))
                        (when (and new-line (<= new-line end))
                          (set! mode "line")
                          (set! end (pred new-line)))

                        (when-debug
                          (destructuring-bind [(?px ?py) (list (render/position))]
                            (affirm (= px (+ (.> window :x) (pred x)))
                                    (= py (+ (.> window :y) (pred y))))))

                        ;; (render/set-back! col)
                        ;; (destructuring-bind [(?px ?py) (list (render/position))]
                        ;;   (import extra/assert (affirm))
                        ;;   (affirm (= px (+ (.> window :x) (pred x)))
                        ;;           (= py (+ (.> window :y) (pred y))))
                        ;;   (render/set-position! 1 (+ ctr 5))
                        ;;   (render/write! (.. mode " " offset " " end " " x))
                        ;;   (render/set-position! px py))

                        (render/write! (string/sub str offset end))
                        (case mode
                          ["curs"
                           (render/set-back! (if (.> window :active) colours/green colours/yellow))
                           (set! x (+ x (- end offset) 2))
                           (case (string/char-at str cursor)
                             ["\n"
                              (render/write! " ")
                              (set! offset (+ end 1))]
                             [?c
                              (render/write! c)
                              (set! offset (+ end 2))])
                           (render/set-back! (expt 2 0))
                           (set! next-cursor math/huge)]

                          ["lend"
                           (set! x 1)
                           (inc! y)
                           (render/set-position! (+ (.> window :x) (pred x)) (+ (.> window :y) (pred y)))
                           (set! offset end)]

                          ["line"
                           (set! x 1)
                           (inc! y)
                           (render/set-position! (+ (.> window :x) (pred x)) (+ (.> window :y) (pred y)))
                           (set! offset (+ end 2))]

                          ["norm"
                           (set! x (+ x (- end offset) 1))
                           (set! offset (succ end))])

                        ;; (destructuring-bind [(?px ?py) (list (render/position))]
                        ;;   (render/set-position! 40 (+ ctr 5))
                        ;;   (render/write! (.. mode " " offset " " end " " x))
                        ;;   (render/set-position! px py))
                        ;; (inc! ctr)
                        ))
                    (set! total-offset (+ total-offset len)))))) })

(defun do-action! (window action)
  "Apply an ACTION to a BUFFER, preserving the previous state."
  (let* [(buffer (.> window :buffer))
         (current (buffer-model buffer))]
    (action window)
    (with (new (buffer-model buffer))
      (when (/= new current)
        ;; Note, we should probably use a doubly linked list here or something,
        ;; to allow for a tree structure. Also store the last modification time of the
        ;; previous one, so we can "group" them together.
        (set-buffer-modn! buffer (succ (buffer-modn buffer)))))))

(defun insert-at! (window offset str)
  "Insert STR into WINDOW's buffer at OFFSET."
  (with (buffer (.> window :buffer))
    (set-buffer-model! buffer (rope/insert (buffer-model buffer) offset str))))

(defun insert! (window str)
  "Insert STR into WINDOW at each cursor position."
  (with (cursors (.> window :cursors))
    (for i (n cursors) 1 -1
      (with (cursor (nth cursors i))
        (insert-at! window cursor str)
        (.<! cursors i (+ cursor (* i (n str))))))))

(defun delete-at! (window offset len)
  "Delete LEN characters in WINDOW's buffer at OFFSET."
  (with (buffer (.> window :buffer))
    (set-buffer-model! buffer (rope/remove (buffer-model buffer) offset (+ offset len -1)))))

(defun delete! (window len)
  "Delete LEN characters in WINDOW at after cursor's position."
  (with (cursors (.> window :cursors))
    (for i (n cursors) 1 -1
      (with (cursor (nth cursors i))
        (delete-at! window cursor len)
        (.<! cursors i (- cursor (* (pred i) len)))))))

(defun delete-before! (window len)
  "Delete LEN characters in WINDOW at before cursor's position."
  (with (cursors (.> window :cursors))
    (for i (n cursors) 1 -1
      (with (cursor (nth cursors i))
        (delete-at! window (- cursor len) len)
        (.<! cursors i (- cursor (* i len)))))))
