(import data/struct ())

(defstruct (buffer buffer-of buffer?)
  "Create a new buffer with the given NAME, containing CONTENTS and the
   specified major MODE."
  (fields
    (immutable name
      "Unique name for this buffer.")

    (mutable model
      "The text model for this buffer.")

    (mutable filename
      "The filename this buffer is stored to, or `nil` if there is no
       corresponding file.")

    (mutable major-mode
      "The major mode this buffer has.")

    (immutable minor-mode
      "List of all minor modes for this buffer.")

    (immutable configuration
      "Collection of buffer specific configuration options.")

    (immutable bindings
      "Collection of buffer local/mode specific keybindings.")

    (mutable modn
      "Current modification number.")

    (mutable save-modn
      "Saved modification number"))

  (constructor new
    (lambda (name model mode)
      (new
        name
        model
        nil
        mode
        '()
        {}
        {}
        0
        0))))
