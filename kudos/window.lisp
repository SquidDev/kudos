(import kudos/logger log)
(import kudos/debug ())

(defun create (parent)
  "Create a new window."
  { :tag "window"

    ;; Stores the outer size and position of this window.
    ;; These are re-calculated whenever the layout changes.
    :x      1 :y      1
    :width  0 :height 0

    ;; Stores the next and previous sibling of this window.
    :parent parent :next nil :previous nil

    ;; Stores the current cursor position
    :cursors '(1)

    ;; Stores whether this is a minibuffer. Minibuffers are only part of the
    ;; top level, and do not have a border.
    :mini false

    ;; hchild is the first horizontal child, vchild is the first vertical child
    ;; buffer is the window's buffer. Only one of these will be non-nil. :mode
    ;; stores which one.
    :mode "none"
    :hchild nil :vchild nil :buffer nil })

(defun set-next! (window next)
  "Set the NEXT window of WINDOW."
  :hidden
  (assert-type! window window)
  (when next
    (assert-type! next window)
    (debug-assert! (= (.> window :parent) (.> next :parent)) "Parents must be equal"))

  (when next (.<! next :previous window))
  (.<! window :next next))

(defun set-previous! (window previous)
  "Set the PREVIOUS window of WINDOW."
  :hidden
  (assert-type! window window)
  (when previous
    (assert-type! previous window)
    (debug-assert! (= (.> window :parent) (.> previous :parent)) "Parents must be equal"))

  (if previous
    (.<! previous :next window)
    (with (parent (.> window :parent))
      (.<! parent (.> parent :mode) window)))
  (.<! window :previous previous))

(defun replace! (from to)
  "Replace FROM with TO inside a window stack. FROM must be an existiing
   window, and TO cannot exist in the stack. TO will be resized to be the
   same size as FROM."
  (assert-type! from window)
  (assert-type! to window)
  (assert-type! (.> from :parent) window)
  (assert-type! (.> to :previous) nil)
  (assert-type! (.> to :next) nil)

  ;; Update the parents of the two objects.
  (.<! to :parent (.> from :parent))
  (.<! from :parent nil)

  ;; Copy dimensions of from to to.
  (.<! to :x (.> from :x))
  (.<! to :y (.> from :y))
  (.<! to :width (.> from :width))
  (.<! to :height (.> from :height))

  (let [(previous (.> from :previous))
        (next (.> from :next))]

    ;; Clear the nodes this points to
    (.<! from :previous nil)
    (.<! from :next nil)

    (set-previous! to previous)
    (set-next! to next)))

(defun resize! (window width height)
  "Resize WINDOW, setting it's size to WIDTH and HEIGHT. This will resize
   all child buffers, as well as re-calculating their positions."

  (.<! window :width width)
  (.<! window :height height)

  (when (/= (.> window :mode) "buffer")
    (let* [(child-width 0)
           (child-height 0)
           (count 0)
           (first (.> window (.> window :mode)))]
      (while first
        (set! child-width (+ child-width (.> first :width)))
        (set! child-height (+ child-height (.> first :height)))
        (inc! count)
        (set! first (.> first :next)))

      (case (.> window :mode)
        ["root"
         (with (child (.> window :root))
           (.<! child :x (.> window :x))
           (.<! child :y (.> window :y))
           (resize! child width height))]
        ["vchild"
         (let* [(y (.> window :y))
                (elem (.> window :vchild))
                (largest elem)]

           ;; Find the largest element, in order to give it the largest height.
           (while elem
             (when (> (.> largest :height) (.> elem :height))
               (set! largest elem))
             (set! elem (.> elem :next)))

           ;; Now actually set positions and sizes
           (set! elem (.> window :vchild))
           (while elem
             (.<! elem :x (.> window :x))
             (.<! elem :y y)
             (resize! elem width (if (= elem largest)
                                   (math/ceil (* (/ (.> elem :height) child-height) height))
                                   (math/floor (* (/ (.> elem :height) child-height) height))))
             (set! y (+ y (.> elem :height)))
             (set! elem (.> elem :next))))]
        ["hchild"
         (let* [(x (.> window :x))
                (elem (.> window :hchild))
                (largest elem)
                (pad-width (succ (- width count)))]

           ;; Find the largest element, in order to give it the largest height.
           (while elem
             (when (> (.> largest :width) (.> elem :width))
               (set! largest elem))
             (set! elem (.> elem :next)))

           ;; Now actually set positions and sizes
           (set! elem (.> window :hchild))
           (while elem
             (.<! elem :x x)
             (.<! elem :y (.> window :y))
             (resize! elem (if (= elem largest)
                             (math/ceil (* (/ (.> elem :width) child-width) pad-width))
                             (math/floor (* (/ (.> elem :width) child-width) pad-width)))
               height)
             (set! x (+ x (succ (.> elem :width))))
             (set! elem (.> elem :next))))]
        ))))


(defun insert-after! (window previous)
  "Insert WINDOW after the PREVIOUS window. Note that this does not
   resize WINDOW or PREVIOUS."
  (assert-type! window window)
  (assert-type! previous window)
  (assert-type! (.> window :previous) nil)
  (assert-type! (.> window :next) nil)
  (assert-type! (.> previous :parent) window)

  ;; Update the parents of the two objects
  (.<! window :parent (.> previous :parent))

  (with (next (.> previous :next))
    (set-previous! window previous)
    (set-next! window next)))

(defun insert-before! (window next)
  "Insert WINDOW before the NEXT window."
  (assert-type! window window)
  (assert-type! next window)
  (assert-type! (.> window :previous) nil)
  (assert-type! (.> window :next) nil)
  (assert-type! (.> next :parent) window)

  (.<! window :parent (.> next :parent))

  (with (previous (.> next :previous))
    (set-previous! window previous)
    (set-next! window next)))

(defun split-vertical! (window)
  (assert-type! window window)

  (with (parent (.> window :parent))
    (when (/= (.> parent :mode) "vchild")
      (set! parent (create))
      (replace! window parent)

      (.<! parent :mode "vchild")
      (.<! parent :vchild window)
      (.<! window :parent parent))

    (let* [(child (create))
           (height (/ (.> window :height) 2))
           (window-height (math/ceil height))
           (child-height (math/floor height))]

      (.<! child :mode "buffer")
      (.<! child :width (.> window :width))
      (.<! child :height child-height)
      (.<! child :x (.> window :x))
      (.<! child :y (+ (.> window :y) window-height))

      (resize! window (.> window :width) window-height)

      (insert-after! child window))))

(defun split-horizontal! (window)
  (assert-type! window window)

  (with (parent (.> window :parent))
    (when (/= (.> parent :mode) "hchild")
      (set! parent (create))
      (replace! window parent)

      (.<! parent :mode "hchild")
      (.<! parent :hchild window)
      (.<! window :parent parent))

    (let* [(child (create))
           (width (/ (.> window :width) 2))
           (window-width (pred (math/ceil width)))
           (child-width (math/floor width))]

      (.<! child :mode "buffer")
      (.<! child :width child-width)
      (.<! child :height (.> window :height))
      (.<! child :x (succ (+ (.> window :x) window-width)))
      (.<! child :y (.> window :y))

      (resize! window window-width (.> window :height))

      (insert-after! child window))))

(defun remove! (window)
  (assert-type! window window)
  (assert-type! (.> window :parent) window)

  (let [(parent (.> window :parent))
        (next (.> window :next))
        (previous (.> window :previous))]

    (if (/= (.> parent :mode) "root")
      (progn
        (if previous
          (.<! previous :next next)
          (.<! parent (.> parent :mode) next))

        (when next (.<! next :previous previous))

        (check-invarient! window)

        (when (or previous next)
          (case (.> parent :mode)
            ["buffer"]
            ["vchild"
             (let [(height (+ (if previous (.> previous :height) 0)
                              (if next (.> next :height) 0)))
                   (extra (.> window :height))]

               (cond
                 [previous
                  (resize! previous
                    (.> previous :width)
                    (+ (.> previous :height) extra))]
                 [next
                  (.<! next :y (- (.> next :y) extra))
                  (resize! next
                    (+ (.> next :height) extra)
                    (.> next :height))]
                 [true]))]

            ["hchild"
             (let [(width (+ (if previous (.> previous :width) 0)
                              (if next (.> next :width) 0)))
                   (extra (succ (.> window :width)))]

               (cond
                 [previous
                  (resize! previous
                    (+ (.> previous :width) extra)
                    (.> previous :height))]
                 [next
                  (.<! next :x (- (.> next :x) extra))
                  (resize! next
                    (+ (.> next :width) extra)
                    (.> next :height))]
                 [true]))]))

        (cond
          [(and previous next) (first-child next)]
          [next
           (unless (.> next :next) (replace! parent next))
           (first-child next)]
          [previous
           (unless (.> previous :previous) (replace! parent previous))
           (first-child previous)]
          [true
           (remove! parent)]))
      window)))

(defun next-win (window)
  (with (next (.> window :next))
    (if next
      (first-child next)
      (with (parent (.> window :parent))
        (if parent
          (next-win parent)
          (first-child window))))))

(defun first-child (window)
  (case (.> window :mode)
    ["buffer" window]
    ["root"   (first-child (.> window :root))]
    ["vchild" (first-child (.> window :vchild))]
    ["hchild" (first-child (.> window :hchild))]))

(defun check-invarient! (window seen pos)
  "Ensures that WINDOW is valid."
  (unless seen (set! seen {}))
  (when (= pos nil) (set! pos true))

  (when (.> seen window) (fail! "Already seen window"))
  (.<! seen window true)

  (case (.> window :mode)
    ["buffer"]
    ["root"
     (with (child (.> window :root))
       (debug-assert! child "root has no child")

       (when pos
         (affirm (= (.> window :x) (.> child :x))
                 (= (.> window :y) (.> child :y))
                 (= (.> window :width) (.> child :width))
                 (= (.> window :height) (.> child :height))))

       (debug-assert! (= (.> child :parent) window) "Parent is different")

       (check-invarient! child seen pos))]
    ["vchild"
     (let* [(child (.> window :vchild))
            (y (.> window :y))]

       (debug-assert! child "vchild has no child")

       (while child
         (when pos
           (affirm (= (.> window :x) (.> child :x))
                   (<= (.> window :y) (.> child :y))
                   (= (.> window :width) (.> child :width))
                   (>= (.> window :height) (.> child :height))))

         (when (.> child :next)
           (affirm (= child (.> child :next :previous))
                   (= (+ (.> child :y) (.> child :height)) (.> child :next :y))))

         (when (.> child :previous)
           (affirm (= child (.> child :previous :next))))

         (debug-assert! (= (.> child :parent) window) "Parent is different")

         (check-invarient! child seen pos)

         (set! child (.> child :next))))]

    ["hchild"
     (let* [(child (.> window :hchild))
            (y (.> window :y))]

       (debug-assert! child "hchild has no child")

       (while child
         (when pos
           (affirm (<= (.> window :x) (.> child :x))
                   (= (.> window :y) (.> child :y))
                   (>= (.> window :width) (.> child :width))
                   (= (.> window :height) (.> child :height))))

         (when (.> child :next)
           (affirm (= child (.> child :next :previous))
                   (= (succ (+ (.> child :x) (.> child :width))) (.> child :next :x))))

         (when (.> child :previous)
           (affirm (= child (.> child :previous :next))))

         (debug-assert! (= (.> child :parent) window) "Parent is different")

         (check-invarient! child seen pos)

         (set! child (.> child :next))))]))
