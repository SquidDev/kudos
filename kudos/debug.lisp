(import test/assert (affirm))

(define debug
  "Determines whether debug assertions are enabled is enabled."
  :hidden
  true)

(defmacro when-debug (&body)
  "Evaluate the expressions in BODY when [[debug]] is true."
  (if debug
    `(progn ,@body)
    `nil))

(defmacro debug-affirm (&assertions)
  "Affirm ASSERTIONS hold when [[debug]] is true."
  (if debug
    `(affirm ,@assertions)
    `nil))

(defmacro debug-assert! (test msg)
  "Assert TEST holds, failing with MSG when [[debug]] is true."
  (if debug
    `(unless ,test (error! ,msg))
    `nil))

(defmacro debug-assert-type! (var type)
  "Assert VAR is of the given TYPE when [[debug]] is true."
  (if debug
    `(assert-type! ,var ,type)
    `nil))
