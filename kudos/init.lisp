(import lua/coroutine co)

(import kudos/buffer ())
(import kudos/buffer/text buffer/text)
(import kudos/input/keybindings (create-key))
(import kudos/input/keybindings keybindings)
(import kudos/input/keys keys)
(import kudos/input/wrap input)
(import kudos/render/colours colours)
(import kudos/render/core render)
(import kudos/render/window render)
(import kudos/model/rope rope)
(import kudos/window window)

(let* [(base (window/create))
       (root (window/create base))
       (mini (window/create))

       (size (list (render/size)))
       (width (nth size 1))
       (height (nth size 2))]

  (.<! base :mode "root")
  (.<! base :width width)
  (.<! base :height (pred height))

  (.<! root :mode "buffer")
  (.<! root :width width)
  (.<! root :height (pred height))
  (.<! root :buffer (buffer-of "foo"
                      (rope/string->rope "Foo bar baz, this is a fooy booy baz which foo boos across the baz. Whilst foo booing it may also baz the fooboo.

                                          And of course, this also fooboos the baz-baz")
                      buffer/text/text-mode))

  (.<! base :root root)
  (.<! root :parent base)

  (.<! mini :mode "buffer")
  (.<! mini :mini true)
  (.<! mini :cursors '())
  (.<! mini :x 1)
  (.<! mini :y height)
  (.<! mini :width width)
  (.<! mini :height 1)
  (.<! mini :buffer (buffer-of "*minibuffer*" (rope/string->rope "Welcome to Kudos!") buffer/text/text-mode))

  (let* [(last-group nil)
         (root-group (keybindings/create-group))
         (group-stack '())
         (running true)]

    (keybindings/add-key! root-group (list
                                       (create-key 45 "c")
                                       (create-key 3))
      (cut window/split-vertical! root))
    (keybindings/add-key! root-group (list
                                       (create-key 45 "c")
                                       (create-key 4))
      (cut window/split-horizontal! root))

    (keybindings/add-key! root-group (list
                                       (create-key 45 "c")
                                       (create-key 11))
      (lambda () (set! root (window/remove! root))))

    (keybindings/add-key! root-group (list
                                       (create-key 45 "c")
                                       (create-key 24))
      (lambda () (set! root (window/next-win root))))

    (keybindings/add-key! root-group (list
                                       (create-key 45 "c")
                                       (create-key 46 "c"))
      (lambda () (set! running false)))

    (keybindings/add-key! root-group (list (create-key 205))
      (lambda () (over! (.> root :cursors) (cut map succ <>))))

    (keybindings/add-key! root-group (list (create-key 203))
      (lambda () (over! (.> root :cursors) (cut map pred <>))))

    (keybindings/add-key! root-group (list (create-key 14))
      (lambda () (buffer/text/do-action! root (cut buffer/text/delete-before! <> 1))))

    (keybindings/add-key! root-group (list (create-key 211))
      (lambda () (buffer/text/do-action! root (cut buffer/text/delete! <> 1))))

    (keybindings/add-key! root-group (list (create-key 28))
      (lambda () (buffer/text/do-action! root (cut buffer/text/insert! <> "\n"))))

    (input/launch (lambda ()
                    (while running
                      ;; TODO: Only rerender the things which need rerendering.
                      (render/set-back! colours/white)
                      (render/clear!)

                      (.<! root :active true)
                      (render/render-window base)
                      (.<! root :active false)

                      (render/render-window mini)

                      (case (list (co/yield))
                        [("key" ?code ?repeat ?ctrl ?meta ?shift)
                         (with (kind (if shift :shifted :normal))
                           (push-cdr! group-stack (cond
                                                    [(and ctrl meta) (.. "C-M-" (or (.> keys/us-qwerty-layout kind code) "?"))]
                                                    [ctrl (.. "C-" (or (.> keys/us-qwerty-layout kind code) "?"))]
                                                    [meta (.. "M-" (or (.> keys/us-qwerty-layout kind code) "?"))]
                                                    [true (or (.> keys/us-qwerty-layout kind code) "?")])))

                         (case (keybindings/handle-key! (or last-group root-group) code ctrl meta)
                           [nil
                            (set! last-group nil)
                            (set! group-stack '())
                            (set-buffer-model! (.> mini :buffer) (rope/string->rope ""))]
                           [false
                            (set! last-group nil)
                            (set-buffer-model! (.> mini :buffer) (rope/string->rope (.. (concat group-stack " ") " is undefined")))
                            (set! group-stack '())]
                           [?x
                            (set! last-group x)
                            (set-buffer-model! (.> mini :buffer) (rope/string->rope (.. (concat group-stack " ") "-")))])

                         (window/check-invarient! base)
                        ]
                        [("char" ?char)
                         (when (.> root :buffer)
                           (buffer/text/do-action! root (cut buffer/text/insert! <> char)))]
                        [_]))))))

(render/set-fore! colours/white)
(render/set-back! colours/black)
(render/clear!)
(render/set-position! 1 1)
