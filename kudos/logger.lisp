(import kudos/filesystem fs)

(fs/delete! "kudos.log")

(defun log! (level msg)
  (assert-type! level string)

  (with (h (fs/open-append! "kudos.log"))
    (fs/write-line! h (string/format "[%s] %s" level (tostring msg)))
    (fs/close! h)))

(define debug! (cut log! "DEBUG" <>))
(define info!  (cut log! "INFO"  <>))
(define warn!  (cut log! "WARN"  <>))
(define error! (cut log! "ERROR" <>))
