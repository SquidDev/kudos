local fn, msg = loadfile(shell.resolve(...), _ENV)
if not fn then error(msg, 0) end

local args = table.pack(...)
local ok, msg = xpcall(function() return fn(unpack(args, 2, args.n)) end, debug.traceback)
if not ok then error(msg, 0) end
