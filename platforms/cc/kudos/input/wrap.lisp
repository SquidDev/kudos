(import lua/coroutine co)
(import lua/debug debug)
(import lua/basic lua)

(import kudos/input/keys keys)

(defun check-resumed! (status result)
  :hidden
  (unless status (fail! result)))

(defmacro do-resume! (co &args)
  :hidden
  `(check-resumed! (co/resume ,co ,@args)))

(defun launch (fn)
  "Run FN, firing additional ctrl_key events."
  (let* [(co (co/create (lambda ()
                          (case (list (xpcall fn debug/traceback))
                            [(false ?msg) (error! msg)]
                            [_]))))
         (keys-down { keys/left-shift false keys/right-shift false
                      keys/left-ctrl  false keys/right-ctrl  false
                      keys/left-alt   false keys/right-alt   false })
         (has-ctrl false)
         (has-shift false)
         (has-meta false)

         (update-modifiers! (lambda ()
                              (set! has-shift (or (.> keys-down keys/left-shift) (.> keys-down keys/right-shift)))
                              (set! has-ctrl  (or (.> keys-down keys/left-ctrl)  (.> keys-down keys/right-ctrl)))
                              (set! has-meta  (or (.> keys-down keys/left-alt)   (.> keys-down keys/right-alt)))))
         (last-key nil)
         (last-timer nil)

         (start-timer! (.> lua/_G :os :startTimer))]

    (do-resume! co)

    (while (/= (co/status co) "dead")
      (with (event (list (co/yield)))
        (case event
          [("key" ?key . _)
           ;; If we've got a buffered key event then flush it
           (when last-key
             (do-resume! co "key" last-key nil has-ctrl has-meta has-shift)
             (set! last-key nil))

           (cond
             [(= (.> keys-down key) false)
              (.<! keys-down key true)
              (update-modifiers!)]
             [else
              (set! last-key key)
              (unless last-timer (set! last-timer (start-timer! 0.05)))])]

          [("key_up" ?key)
           ;; If we've got a buffered key event then flush it
           (when last-key
             (do-resume! co "key" last-key nil has-ctrl has-meta has-shift)
             (set! last-key nil))

           (when (.> keys-down key)
             (.<! keys-down key false)
             (update-modifiers!))]

          [("char" ?char)
           ;; If we've got a buffered key then merge events
           (when last-key
             (do-resume! co "key" last-key char has-ctrl has-meta has-shift)
             (set! last-key nil))]

          [("timer" ?id)
           ;; If we've got a timer then flush the previous key
           (when (= id last-timer)
             (set! last-timer nil)
             (when last-key
               (do-resume! co "key" last-key nil has-ctrl has-meta has-shift)
               (set! last-key nil)))]

          [_])

        (do-resume! co "event" event)))))
