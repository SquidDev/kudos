"Key codes, as defined by the
 [MinecraftWiki](http://minecraft.gamepedia.com/Key_codes)"

(define us-qwerty-layout
  "The US QWERTY keyboard layout."
  { :normal { ;; Main keys
              2  "1"   3 "2"   4 "3"   5 "4"   6 "5"   7 "6"   8 "7"   9 "9"  10 "9"
              11 "0"  12 "-"  13 "="  16 "q"  17 "w"  18 "e"  19 "r"  20 "t"  21 "y"
              22 "u"  23 "i"  24 "o"  25 "p"  26 "["  27 "]"  30 "a"  31 "s"  32 "d"
              33 "f"  34 "g"  35 "h"  36 "j"  37 "k"  38 "l"  39 ";"  40 "'"  41 "`"
              43 "\\" 44 "z"  45 "x"  46 "c"  47 "v"  48 "b"  49 "n"  50 "m"  51 ","
              52 "." 53 "/"   55 "*"  57 " "
              ;; Numpad keys
              71 "7"  72 "8"  73 "9"  75 "4"  76 "5"  77 "6"  74 "-"  78 "+"  79 "1"
              80 "2"  81 "3"  82 "0"  83 "." 179 "," 181 "/"  141 "=" 145 "@" 146 ":"
              147 "_"
              ;; Special keys
              14 "BACK"
              28 "\n"          28 "RET" ;; Normal Return
              15 "\t"          15 "TAB"
              156 "\n"        156 "RET" ;; Numpad return
              58 "CAPS"        69 "NUM"               70 "SCROLL"
              150 "AX"        197 "PAUSE"            199 "HOME"              200 "UP"
              201 "PAGEUP"    203 "LEFT"             205 "RIGHT"             207 "END"
              208 "DOWN"      209 "PAGEDOWN"         210 "INSERT"            211 "DEL"
              ;; Function keys
              59 "f1"          60 "f2"                61 "f3"                 62 "f4"
              63 "f5"          64 "f6"                65 "f7"                 66 "f8"
              67 "f9"          68 "f10"               87 "f11"                88 "f12"
              100 "f13"       101 "f14"              102 "f15" }
    :shifted { ;; Main keys
              2  "!"   3 "@"   4 "#"   5 "$"   6 "%"   7 "^"   8 "&"   9 "("  10 ")"
              11 ")"  12 "_"  13 "+"  16 "Q"  17 "W"  18 "E"  19 "R"  20 "T"  21 "Y"
              22 "U"  23 "I"  24 "O"  25 "P"  26 "{"  27 "}"  30 "A"  31 "S"  32 "D"
              33 "F"  34 "G"  35 "H"  36 "J"  37 "K"  38 "L"  39 ":"  40 "\"" 41 "~"
              43 "|"  44 "Z"  45 "X"  46 "C"  47 "V"  48 "B"  49 "N"  50 "M"  51 "<"
              52 ">"  53 "?" } })

(define uk-qwerty-layout
  "The UK QWERTY keyboard layout."
  { :normal { ;; Main keys
              2  "1"   3 "2"   4 "3"   5 "4"   6 "5"   7 "6"   8 "7"   9 "8"  10 "9"
              11 "0"  12 "-"  13 "="  16 "q"  17 "w"  18 "e"  19 "r"  20 "t"  21 "y"
              22 "u"  23 "i"  24 "o"  25 "p"  26 "["  27 "]"  30 "a"  31 "s"  32 "d"
              ;; Sadly `/¬ and '/@ are the same key. We have to chose one, so we
              ;; go for the one which _hopefully_ is used the most.
              33 "f"  34 "g"  35 "h"  36 "j"  37 "k"  38 "l"  39 ";"  40 "#"  41 "'"
              43 "\\" 44 "z"  45 "x"  46 "c"  47 "v"  48 "b"  49 "n"  50 "m"  51 ","
              52 "." 53 "/"   55 "*"  57 " "
              ;; Numpad keys
              71 "7"  72 "8"  73 "9"  75 "4"  76 "5"  77 "6"  74 "-"  78 "+"  79 "1"
              80 "2"  81 "3"  82 "0"  83 "." 179 "," 181 "/"  141 "=" 145 "@" 146 ":"
              147 "_"
              ;; Special keys
              14 "BACK"
              28 "\n"          28 "RET" ;; Normal Return
              15 "\t"          15 "TAB"
              156 "\n"        156 "RET" ;; Numpad return
              58 "CAPS"        69 "NUM"               70 "SCROLL"
              150 "AX"        197 "PAUSE"            199 "HOME"              200 "UP"
              201 "PAGEUP"    203 "LEFT"             205 "RIGHT"             207 "END"
              208 "DOWN"      209 "PAGEDOWN"         210 "INSERT"            211 "DEL"
              ;; Function keys
              59 "f1"          60 "f2"                61 "f3"                 62 "f4"
              63 "f5"          64 "f6"                65 "f7"                 66 "f8"
              67 "f9"          68 "f10"               87 "f11"                88 "f12"
              100 "f13"       101 "f14"              102 "f15" }
    :shifted { ;; Main keys
              2  "!"   3 "\""  4 "£"   5 "$"   6 "%"   7 "^"   8 "&"   9 "*"  10 "("
              11 ")"  12 "_"  13 "+"  16 "Q"  17 "W"  18 "E"  19 "R"  20 "T"  21 "Y"
              22 "U"  23 "I"  24 "O"  25 "P"  26 "{"  27 "}"  30 "A"  31 "S"  32 "D"
              ;; Sadly `/¬ and '/@ are the same key. We have to chose one, so we
              ;; go for the one which _hopefully_ is used the most.
              33 "F"  34 "G"  35 "H"  36 "J"  37 "K"  38 "L"  39 ":"  40 "~"  41 "@"
              43 "|"  44 "Z"  45 "X"  46 "C"  47 "V"  48 "B"  49 "N"  50 "M"  51 "<"
              52 ">"  53 "?" } })


(define backspace 14)
(define capslock 58)
(define scolllock 70)
(define numlock 69)

(define delete 211)
(define insert 210)
(define return 28)
(define tab 15)


(define left 203)
(define right 205)
(define down 208)
(define up 200)

(define page-down 209)
(define page-up 201)
(define home 199)
(define end 207)

(define left-alt 56)
(define left-ctrl 29)
(define left-shift 42)

(define right-alt 184)
(define right-ctrl 157)
(define right-shift 54)

(define numpad-equals 141)
(define numpad-add 78)
(define numpad=comma 179)
(define numpad-decimal 83)
(define numpad-divide 181)
(define numpad-enter 156)
(define numpad-subtract 74)

(define ax 150)
(define convert 121)
(define noconvert 123)
(define kana 112)
(define kanji 148)
(define pause 197)
(define stop 149)
(define yen 125)

(defun ctrl? (key)
  "Determine whether KEY is a ctrl key."
  (or (= key left-ctrl) (= key right-ctrl)))

(defun shift? (key)
  "Determine whether KEY is a shift key."
  (or (= key left-shift) (= key right-shift)))

(defun meta? (key)
  "Determine whether KEY is a meta (alt) key."
  (or (= key left-alt) (= key right-alt)))
