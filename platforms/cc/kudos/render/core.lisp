(define-native set-fore! "Set the current foreground COLOUR.")
(define-native set-back! "Set the current background COLOUR.")

(define-native fore "Get the current foreground colour.")
(define-native back "Get the current background colour.")

(define-native position "Get the current cursor position.")
(define-native set-position! "Set the current cursor position to X and Y.")

(define-native set-blink-on! :hidden)

(defun set-blink! (character)
  "Set the CHARACTER to blink with."
  (set-blink-on! (if character (/= character " ") character)))

(define-native size "Get the current terminal size.")

(define-native write! "Write a STRING to the terminal.")

(define-native clear! "Clear the current terminal.")