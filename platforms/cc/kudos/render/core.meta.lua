local term = term or {}
return {
	["back"] =          { tag = "var", contents = "term.getBackgroundColour",  value = term.getBackgroundColour, },
	["fore"] =          { tag = "var", contents = "term.getTextColour",        value = term.getTextColour,       },
	["position"] =      { tag = "var", contents = "term.getCursorPos",         value = term.getCursorPos,        },
	["set-back!"] =     { tag = "var", contents = "term.setBackgroundColour",  value = termsetBackgroundColour,  },
	["set-fore!"] =     { tag = "var", contents = "term.setTextColour",        value = term.setTextColour        },
	["set-position!"] = { tag = "var", contents = "term.setCursorPos",         value = term.setCursorPos,        },
	["set-blink-on!"] = { tag = "var", contents = "term.setBlink",             value = term.setBlink,            },
	["size"] =          { tag = "var", contents = "term.getSize",              value = term.getSize,             },
	["write!"] =        { tag = "var", contents = "term.write",                value = term.write,               },
	["clear!"] =        { tag = "var", contents = "term.clear",                value = term.clear,               },
}
