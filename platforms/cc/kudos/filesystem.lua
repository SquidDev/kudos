local fs, select = fs or {}, select
return {
	combine = function(x, y, ...)
		local main = fs.combine(x, y)
		for i = 1, select('#', ...) do main = fs.combine(main, select(i, ...)) end
		return main
	end,
}