local fs = fs or {}
return {
	["copy!"] =           { tag = "var", contents = "fs.copy",                 value = fs.copy,               },
	["delete!"] =         { tag = "var", contents = "fs.delete",               value = fs.delete,             },
	["dir"] =             { tag = "var", contents = "fs.getDir",               value = fs.getDir,             },
	["directory?"] =      { tag = "var", contents = "fs.isDir",                value = fs.isDir,              },
	["exists?"] =         { tag = "var", contents = "fs.exists",               value = fs.exists,             },
	["list"] =            { tag = "var", contents = "fs.list",                 value = fs.list,               },
	["mkdir!"] =          { tag = "var", contents = "fs.makeDir",              value = fs.makeDir,            },
	["move!"] =           { tag = "var", contents = "fs.move",                 value = fs.move,               },
	["open!"] =           { tag = "var", contents = "fs.open",                 value = fs.open,               },
	["name"] =            { tag = "var", contents = "fs.getName",              value = fs.getName,            },
	["size"] =            { tag = "var", contents = "fs.getSize",              value = fs.getSize,            },
}
