(define-native combine
  "Combine each element in PATHs into one.")

(defun canonical (path)
  "Determines the cannonical form of PATH. This removes all
   \"indirection\" like \"..\" and \".\"."
  (combine path ""))

(define-native dir
  "Get the directory part of PATH.")

(define-native name
  "Get the file part of PATH.")

(define-native exists?
  "Check PATH exists.")

(define-native directory?
  "Check whether PATH is a directory.")

(define-native size
  "Get the size of PATH, returning 0 if it does not point to a file.")

(define-native list
  "List all files in PATH.")

(define-native mkdir!
  "Make the directory PATH. This will also make all parent directories.")

(define-native delete!
  "Delete PATH. This will also delete all child files and directories.")

(define-native move!
  "Move a file or folder FROM one location TO another.")

(define-native copy!
  "Copy a file or folder FROM one location TO another.")

(define-native open! :hidden)

(defun open-read (path)
  "Open PATH for reading, returns nil and an error message if it cannot
   be opened."
  (open! path "r"))

(defun open-write! (path)
  "Open PATH for writing, returns nil and an error message if it cannot
   be opened."
  (open! path "w"))

(defun open-append! (path)
  "Open PATH for appending, returns nil and an error message if it cannot
   be opened."
  (open! path "a"))

(defun open-bin-read! (path)
  "Open PATH for reading in binary mode, returns nil and an error
   message if it cannot be opened."
  (open! path "wb"))

(defun open-bin-write! (path)
  "Open PATH for writing in binary mode, returns nil and an error
   message if it cannot be opened."
  (open! path "wb"))

(defun read! (handle)
  "Read the file contents from HANDLE."
  ((.> handle :readAll)))

(defun read-line! (handle)
  "Read a line from HANDLE."
  ((.> handle :readLine)))

(defun write (handle string)
  "Write a STRING to HANDLE."
  ((.> handle :write) string))

(defun write-line! (handle line)
  "Write a LINE to HANDLE."
  ((.> handle :writeLine) line))

(defun close! (handle)
  "Close the given file HANDLE."
  ((.> handle :close)))

(defun flush! (handle)
  "Flush the given file HANDLE."
  ((.> handle :flush)))
